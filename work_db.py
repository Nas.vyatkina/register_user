import traceback
import psycopg2
from config import create_connection
import logging
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)

log_formatter = logging.Formatter('%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')

my_handler = RotatingFileHandler("example1.log", maxBytes=5 * 1024 * 1024, backupCount=10)
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.ERROR)

logger.addHandler(my_handler)


# добавление нового пользователя в БД
def db_register_user(info_user):
    conn = create_connection()
    status = 1
    id_user = -1

    user_json = {
        "name": None,
        "email": None,
        "isSocial": None,
        "idCourseFaculty": None
    }
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            INSERT INTO 
                                    users (
                                            name, 
                                            email, 
                                            password_hash, 
                                            id_course_faculty
                                            )
                            SELECT
                                    '{}',
                                    '{}',
                                    '{}',
                                    id_course_faculty
                            FROM course_faculty
                            WHERE ref_id_course = {} AND ref_id_faculty={}
                            RETURNING name, email, is_social, id_course_faculty, id_user       
                        '''.format(
                                    info_user['name'],
                                    info_user['email'],
                                    info_user['password'],
                                    info_user['course'],
                                    info_user['faculty']
                                ))
            user = cur.fetchone()
            if not user:
                status = -2
                cur.close()
                conn.close()
                return status, user_json, id_user

        except psycopg2.IntegrityError as e:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            if 'email_u' in e.pgerror:
                status = 0
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            #print(traceback.format_exc())
            status = -1
        else:
            user_json["name"] = user[0]
            user_json["email"] = user[1]
            user_json["isSocial"] = user[2]
            user_json["idCourseFaculty"] = user[3]
            id_user = user[4]
            conn.commit()
        cur.close()
        conn.close()
    else:
        status = -1
    return status, user_json, id_user


# данные юзера из БД при логине - email, password
def db_info_user_to_inter(email, password):
    conn = create_connection()
    status = 1
    user_json = {
        "idUser": None,
        "name": None,
        "email": None,
        "passwordHash": None,
        "isSocial": None,
        "idCourseFaculty": None
    }
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT
                                    id_user,
                                    name,
                                    email,
                                    password_hash,
                                    is_social,
                                    id_course_faculty
                            FROM users
                            WHERE (
                                  email = '{}' 
                                  AND password_hash = '{}'
                                  )
                        '''.format(
                                    email,
                                    password
                                    ))
            user = cur.fetchone()
            if not user:
                status = 0
                cur.close()
                conn.close()
                return status, user_json
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = -1
        else:
            user_json["idUser"] = user[0]
            user_json["name"] = user[1]
            user_json["email"] = user[2]
            user_json["passwordHash"] = user[3]
            user_json["isSocial"] = user[4]
            user_json["idCourseFaculty"] = user[5]
        cur.close()
        conn.close()
    else:
        status = -1
    return status, user_json


# данные юзера из БД по id
def db_info_user(id_user):
    conn = create_connection()
    status = 1
    user_json = {
        "name": None,
        "email": None,
        "passwordHash": None,
        "isSocial": None,
        "idCourseFaculty": None
    }
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT
                                    name,
                                    email,
                                    is_social,
                                    id_course_faculty
                            FROM users
                            WHERE (
                                  id_user = {}
                                  )
                        '''.format(
                                    id_user
                                    ))
            user = cur.fetchone()
            if not user:
                status = 0
                cur.close()
                conn.close()
                return status, user_json
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = -1
        else:
            user_json["name"] = user[0]
            user_json["email"] = user[1]
            user_json["isSocial"] = user[2]
            user_json["idCourseFaculty"] = user[3]
        cur.close()
        conn.close()
    else:
        status = -1
    return status, user_json


# обновление данных юзера (передаются только те параметры, которые нужно изменить)
def db_update_info_user_1(id_user, data):
    conn = create_connection()
    val = []
    id_course = None
    id_faculty = None

    info_user_json = {
                        "name": None,
                        "email": None,
                        "isSocial": None,
                        "idCourseFaculty": None
                    }
    status = 1
    if conn:
        cur = conn.cursor()
        try:
            if data.get('course') and not data.get('faculty'):
                cur.execute('''
                                WITH cf AS(
                                            SELECT 
                                                    ref_id_faculty, 
                                                    id_course_faculty
                                            FROM course_Faculty)
                                SELECT 
                                        cf.ref_id_faculty
                                FROM cf
                                INNER JOIN users ON cf.id_course_faculty = users.id_course_faculty
                                WHERE id_user = {}
                            '''.format(
                                        id_user
                                    ))
                id_faculty = cur.fetchone()[0]
                id_course = data['course']
            elif data.get('faculty') and not data.get('course'):
                cur.execute('''
                                WITH cf AS(
                                            SELECT 
                                                    ref_id_course, 
                                                    id_course_faculty
                                            FROM course_Faculty)
                                SELECT cf.ref_id_course
                                FROM cf
                                INNER JOIN users ON cf.id_course_faculty = users.id_course_faculty
                                WHERE id_user = {}
                            '''.format(
                                        id_user
                                    ))
                id_course = cur.fetchone()[0]
                id_faculty = data['faculty']
            elif data.get('course') and data.get('faculty'):
                id_course = data['course']
                id_faculty = data['faculty']
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = -1
            cur.close()
            conn.close()
            return status, info_user_json
        try:
            if data.get('name'):
                val.append(" name = '" + str(data['name']) + "' ")
            if data.get('email'):
                val.append(" email = '" + str(data['email']) + "' ")
            if data.get('password'):
                val.append(" password_hash = '" + str(data['password']) + "' ")

            if val and not id_course:
                cur.execute('''
                               UPDATE 
                                    users
                               SET
                                    {}
                               WHERE id_user = {}
                               RETURNING name, email, is_social, id_course_faculty      
                            '''.format(
                                        ",".join(val),
                                        id_user
                                    ))
                info_user = cur.fetchone()
            elif val and id_course:
                cur.execute('''
                               UPDATE 
                                    users
                               SET
                                    {},
                                    id_course_faculty = (SELECT id_course_faculty 
                                                         FROM course_faculty
                                                         WHERE
                                                                ref_id_course = {} and
                                                                ref_id_faculty = {})
                               WHERE id_user = {}
                               RETURNING name, email, is_social, id_course_faculty      
                            '''.format(
                                        ",".join(val),
                                        id_course,
                                        id_faculty,
                                        id_user
                                    ))
                info_user = cur.fetchone()
            elif not val and id_course:
                cur.execute('''
                               UPDATE 
                                    users
                               SET
                                    id_course_faculty = (SELECT id_course_faculty 
                                                         FROM course_faculty
                                                         WHERE
                                                                ref_id_course = {} and
                                                                ref_id_faculty = {})
                               WHERE id_user = {}
                               RETURNING name, email, is_social, id_course_faculty      
                            '''.format(
                                        id_course,
                                        id_faculty,
                                        id_user
                                    ))
                info_user = cur.fetchone()
        except psycopg2.IntegrityError as e:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            if 'email_u' in e.pgerror:
                status = 0
            elif 'course_faculty' in e.pgerror:
                status = -2
            cur.close()
            conn.close()
            return status, info_user_json
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = -1
            cur.close()
            conn.close()
            return status, info_user_json
        else:

            info_user_json["name"] = info_user[0]
            info_user_json["email"] = info_user[1]
            info_user_json["isSocial"] = info_user[2]
            info_user_json["idCourseFaculty"] = info_user[3]
            conn.commit()

        cur.close()
        conn.close()
        print(info_user_json)
    else:
        status = -1
    return status, info_user_json


# обновление данных юзера (передается полная структура с параметрами)
def db_update_info_user_2(id_user, data):
    conn = create_connection()
    info_user_json = {
        "name": None,
        "email": None,
        "isSocial": None,
        "idCourseFaculty": None
    }
    status = 1
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            UPDATE
                                users
                            SET 
                                name = '{}',
                                email = '{}',
                                password_hash = '{}',
                                id_course_faculty = (SELECT id_course_faculty 
                                                         FROM course_faculty
                                                         WHERE
                                                                ref_id_course = {} and
                                                                ref_id_faculty = {})
                            WHERE id_user = {}
                            RETURNING name, email, is_social, id_course_faculty      
                        '''.format(
                                        data['name'],
                                        data['email'],
                                        data['password'],
                                        data['course'],
                                        data['faculty'],
                                        id_user
                                    ))

            info_user = cur.fetchone()
        except psycopg2.IntegrityError as e:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            if 'email_u' in e.pgerror:
                status = 0
            elif 'course_faculty' in e.pgerror:
                status = -2
            cur.close()
            conn.close()
            return status, info_user_json
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = -1
            cur.close()
            conn.close()
            return status, info_user_json
        else:

            info_user_json["name"] = info_user[0]
            info_user_json["email"] = info_user[1]
            info_user_json["isSocial"] = info_user[2]
            info_user_json["idCourseFaculty"] = info_user[3]
            conn.commit()

        cur.close()
        conn.close()
        #print(info_user_json)
    else:
        status = -1
    return status, info_user_json


def verification_of_exist_faculty(id_user, id_course_faculty):
    conn = create_connection()
    status = 1
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            WITH 
                                u AS (
                                    SELECT 
                                          id_course_faculty, 
                                          id_user
                                    FROM users
                                    WHERE 
                                        id_course_faculty = {}
                                    AND id_user = {}
                                    ),    
                                ur AS (
                                    SELECT 
                                          f.id_faculty, 
                                          cf.id_course_faculty, 
                                          cf.ref_id_faculty
                                    FROM faculty f
                                    INNER JOIN course_faculty cf ON f.id_faculty = cf.ref_id_faculty
                                    )
                            SELECT 
                                  id_faculty
                            FROM ur
                            INNER JOIN u ON ur.id_course_faculty = u.id_course_faculty
 
                        '''.format(
                                    id_course_faculty,
                                    id_user
                                ))
            faculty = cur.fetchone()
            if not faculty:
                status = 0
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = -1
        cur.close()
        conn.close()

    else:
        status = -1
    return status
import redis

r = redis.StrictRedis(host='localhost', port=6379)


# добавление пары sessionKey: idUser в Redis
def set_session_key(session_key, id_user):
    r.set(session_key, id_user)
    return 0


# извлечение idUser по сессионному ключу из Redis
def get_id_user(session_key):
    try:
        if r.get(session_key):
            return int(r.get(session_key).decode('utf-8'))
    except KeyError:
        return -1
    else:
        return -1


# удаление сессионного ключа при логауте
def del_session_key(session_key):
    r.delete(session_key)
    return 0
from flask import Flask, request, jsonify
import datetime
import requests
from binascii import hexlify
from hashlib import sha256
from os import urandom
from re import match
from work_db import db_register_user, db_info_user_to_inter, db_update_info_user_1, db_update_info_user_2, \
    db_info_user, verification_of_exist_faculty
from work_redis import set_session_key, get_id_user, del_session_key
app = Flask(__name__)

api_user = "/api/v1/user"


# проверка правильности написания имейла
def check_email(email):
    if not match("[\\w._%+-|]+@[\\w0-9.-]+\\.[A-Za-z]{2,6}", email):
        return False
    return True


@app.route(api_user + '/register', methods=["POST"])
def register_user():
    # json с введенной информацией на экране регистрации
    register_info = request.json

    """   
    структура входящего файла
    register_info = {
                        "name": "Egor",
                        "email": "test6@mail.ru",
                        "password": "12345",
                        "course": 2,
                        "faculty": 1
                    }
    
    структура ответа
      {
        "code": 1,
        "message": "",
        "data": 
              {
                  "email": "test@gmail.com",
                  "idCourseFaculty": 5,
                  "name": "dasd",
                  "isSocial": True/False
              }
        }
        code:
        1 - успешно
        0 - ошибки при вводе (пустые входящие данные/неправильно введены данные)
        -1 - имейл уже зарегистрирован
        -2 - ошибка при работе с БД
        -4 - отсутствие ключа
        при возникновении ошибки "data": None
    """

    keys_in_input_json = register_info.keys()
    if not 'name' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'name'",
                  "data": None}
        return jsonify(answer)
    if not 'email' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'email'",
                  "data": None}
        return jsonify(answer)
    if not 'password' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'password'",
                  "data": None}
        return jsonify(answer)
    if not 'course' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'course'",
                  "data": None}
        return jsonify(answer)
    if not 'faculty' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'faculty'",
                  "data": None}
        return jsonify(answer)

    if not register_info['faculty']:
        answer = {"code": 0,
                  "message": "field 'faculty' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(register_info['faculty']) == int:
        answer = {"code": 0,
                  "message": str("invalid data type: [faculty]"),
                  "data": None}
        return jsonify(answer)

    if not register_info['course']:
        answer = {"code": 0,
                  "message": "field 'course' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(register_info['course']) == int:
        answer = {"code": 0,
                  "message": str("invalid data type: [course]"),
                  "data": None}
        return jsonify(answer)

    if not register_info['name']:
        answer = {"code": 0,
                  "message": "field 'name' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(register_info['name']) == str:
        answer = {"code": 0,
                  "message": str("invalid data type: [name]"),
                  "data": None}
        return jsonify(answer)
    elif len(register_info['name']) > 100:
        answer = {"code": 0,
                  "message": "the length of the received data of the 'name' field exceeds the permissible value",
                  "data": None}
        return jsonify(answer)

    if not register_info['password']:
        answer = {"code": 0,
                  "message": "field 'password' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(register_info['password']) == str:
        answer = {"code": 0,
                  "message": str("invalid data type: [password]"),
                  "data": None}
        return jsonify(answer)
    elif len(register_info['password']) > 50:
        answer = {"code": 0,
                  "message": "the length of the received data of the 'password' field exceeds the permissible value",
                  "data": None}
        return jsonify(answer)

    if not register_info['email']:
        answer = {"code": 0,
                  "message": "field 'email' is empty",
                  "data": None}
        return jsonify(answer)

    # проверка на корректный ввод имейла
    if not check_email(register_info['email']):
        answer = {"code": 0,
                  "message": "incorrect input of the 'email' field",
                  "data": None}
        return jsonify(answer)
    elif len(register_info['email']) > 200:
        answer = {"code": 0,
                  "message": "the length of the received data of the 'email' field exceeds the permissible value",
                  "data": None}
        return jsonify(answer)

    status, info_user, id_user = db_register_user(info_user=register_info)
    if status == -2:
        answer = {"code": 0,
                  "message": "the chosen faculty has no courses/course or faculty is Null",
                  "data": None}
        return jsonify(answer)
    if status == -1:
        answer = {"code": -2,
                  "message": "error while working with DB",
                  "data": None}
        return jsonify(answer)
    # проверка на наличие введенного имейла в БД, если имейл существует - выдать ошибку
    elif status == 0:
        answer = {"code": -1,
                  "message": "email already exists",
                  "data": None}
        return jsonify(answer)
    elif status == 1:
        data = {"code": 1,
                "message": "ok",
                "data": info_user}

    now = datetime.datetime.now()
    session_key = sha256(bytes(register_info['name'] + str(now), 'utf-8') + hexlify(urandom(22))).hexdigest()

    set_session_key(session_key, id_user)
    answer = jsonify(data)
    answer.headers.extend({'sessionKey': session_key})

    print(answer.headers.get('sessionKey'))
    return answer


@app.route(api_user + '/login', methods=["POST"])
def login_user():
    # json с введенной информацией на экране входа
    login_info = request.json

    '''
    структура входного json файла
    {
    "email": "test@gmail.com"
    "password": "test"
    }
    
    
    структура ответа
    
    при возникновении ошибки
    {"code": -4,
    "message": "in the input json there is no key 'password'",
    "data": None}
    
    0 - ошибки при вводе(пустые данные / несоответствие типов)
    -1 - пользователя с введенными данными не существует
    -2 - ошибка при работе с БД
    -4 - отсутсвие ключа
    '''
    # перенести в другой метод позже
    # session_key = login_info.headers.get('sessionKey')
    # if not session_key:
    #    answer = {"code": -4,
    #              "message": "invalid sessionKey",
    #              "data": None}
    #   return answer
    keys_in_input_json = login_info.keys()
    if not 'password' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'password'",
                  "data": None}
        return jsonify(answer)
    if not 'email' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'email'",
                  "data": None}
        return jsonify(answer)

    if not login_info['password']:
        answer = {"code": 0,
                  "message": "field 'password' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(login_info['password']) == str:
        answer = {"code": 0,
                  "message": str("invalid data type: [password]"),
                  "data": None}
        return jsonify(answer)

    if not login_info['email']:
        answer = {"code": 0,
                  "message": "field 'email' is empty",
                  "data": None}
        return jsonify(answer)


    status, info_user = db_info_user_to_inter(login_info["email"], login_info["password"])
    if status == -1:
        answer = {"code": -2,
                  "message": "error while working with DB",
                  "data": None}
        return jsonify(answer)
    elif status == 0:
        answer = {"code": -1,
                  "message": "user with such email/password does not exist",
                  "data": None}
        return jsonify(answer)

    elif status == 1:
        data = {
            "code": 1,
            "message": "ok",
            "data": {"name": info_user["name"],
                     "email": info_user["email"],
                     "isSocial": info_user["isSocial"],
                     "idCourseFaculty": info_user["idCourseFaculty"]}
            }

    status = verification_of_exist_faculty(info_user['idUser'], info_user['idCourseFaculty'])
    if status == -1:
        answer = {"code": -2,
                  "message": "error while working with DB",
                  "data": None}
        return jsonify(answer)
    elif status == 0:
        answer = {"code": -1,
                  "message": "chosen faculty doesn't exist",
                  "data": None}
        return jsonify(answer)

    now = datetime.datetime.now()
    session_key = sha256(bytes(info_user['name'] + str(now), 'utf-8') + hexlify(urandom(22))).hexdigest()

    set_session_key(session_key, info_user["idUser"])
    answer = jsonify(data)
    answer.headers.extend({'sessionKey': session_key})
    print(answer.headers.get('sessionKey'))

    return answer


@app.route(api_user + '/info')
def info_user():
    s_key = request.headers.get('sessionKey')
    id_user = get_id_user(s_key)
    if id_user == -1:
        answer = {"code": 0,
                  "message": "invalid sessionKey",
                  "data": None}
        return jsonify(answer)

    status, info_user = db_info_user(id_user)
    if status == -1:
        answer = {"code": -2,
                  "message": "error while working with DB",
                  "data": None}
        return jsonify(answer)
    elif status == 0:
        answer = {"code": -1,
                  "message": "user with such id does not exist",
                  "data": None}
        return jsonify(answer)

    elif status == 1:
        answer = {
            "code": 1,
            "message": "ok",
            "data": info_user
            }

    return jsonify(answer)


@app.route(api_user + '/update/1', methods=['POST'])
def update_info_user_1():
    info_update = request.json
    s_key = request.headers.get('sessionKey')
    id_user = get_id_user(s_key)
    if id_user == -1:
        answer = {"code": 0,
                  "message": "invalid sessionKey",
                  "data": None}
        return jsonify(answer)

    keys = info_update.keys()
    message = ''
    for key in keys:
        if not info_update[key]:
            message = message + "'" + key + "',"
    if message:
        answer = {"code": 0,
                  "message": "field is empty: " + message[:-1],
                  "data": None}
        return jsonify(answer)

    for key in keys:
        if key == "course" or key == "faculty":
            if not type(info_update[key]) == int:
                answer = {"code": 0,
                          "message": str("invalid data type: [" + key + "]"),
                          "data": None}
                return jsonify(answer)
        else:
            if not type(info_update[key]) == str:
                answer = {"code": 0,
                          "message": str("invalid data type: [" + key + "]"),
                          "data": None}
                return jsonify(answer)

    if info_update.get('email'):
        if not check_email(info_update['email']):
            answer = {"code": 0,
                      "message": "incorrect input of the 'email' field",
                      "data": None}
            return jsonify(answer)
        elif len(info_update['email']) > 200:
            answer = {"code": 0,
                      "message": "the length of the received data of the 'email' field exceeds the permissible value",
                      "data": None}
            return jsonify(answer)

    if info_update.get('password'):
        if len(info_update['password']) > 50:
            answer = {"code": 0,
                      "message": "the length of the received data of the 'password' field exceeds the permissible value",
                      "data": None}
            return jsonify(answer)

    if info_update.get('name'):
        if len(info_update['name']) > 100:
            answer = {"code": 0,
                      "message": "the length of the received data of the 'name' field exceeds the permissible value",
                      "data": None}
            return jsonify(answer)

    status, info_user = db_update_info_user_1(id_user, info_update)

    if status == -2:
        answer = {"code": 0,
                  "message": "the chosen faculty has no courses/course or faculty is Null",
                  "data": None}
        return jsonify(answer)
    if status == -1:
        answer = {"code": -2,
                  "message": "error while working with DB",
                  "data": None}
        return jsonify(answer)
    # проверка на наличие введенного имейла в БД, если имейл существует - выдать ошибку
    elif status == 0:
        answer = {"code": -1,
                  "message": "email already exists",
                  "data": None}
        return jsonify(answer)
    elif status == 1:
        answer = {"code": 1,
                  "message": "ok",
                  "data": info_user}

    return jsonify(answer)


@app.route(api_user + '/update/2', methods=['POST'])
def update_info_user_2():
    info_update = request.json
    s_key = request.headers.get('sessionKey')
    id_user = get_id_user(s_key)
    if id_user == -1:
        answer = {"code": 0,
                  "message": "invalid sessionKey",
                  "data": None}
        return jsonify(answer)

    keys_in_input_json = info_update.keys()
    if not 'name' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'name'",
                  "data": None}
        return jsonify(answer)
    if not 'email' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'email'",
                  "data": None}
        return jsonify(answer)
    if not 'password' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'password'",
                  "data": None}
        return jsonify(answer)
    if not 'course' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'course'",
                  "data": None}
        return jsonify(answer)
    if not 'faculty' in keys_in_input_json:
        answer = {"code": -4,
                  "message": "in the input json there is no key 'faculty'",
                  "data": None}
        return jsonify(answer)

    if not info_update['faculty']:
        answer = {"code": 0,
                  "message": "field 'faculty' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(info_update['faculty']) == int:
        answer = {"code": 0,
                  "message": str("invalid data type: [faculty]"),
                  "data": None}
        return jsonify(answer)

    if not info_update['course']:
        answer = {"code": 0,
                  "message": "field 'course' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(info_update['course']) == int:
        answer = {"code": 0,
                  "message": str("invalid data type: [course]"),
                  "data": None}
        return jsonify(answer)

    if not info_update['name']:
        answer = {"code": 0,
                  "message": "field 'name' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(info_update['name']) == str:
        answer = {"code": 0,
                  "message": str("invalid data type: [name]"),
                  "data": None}
        return jsonify(answer)
    elif len(info_update['name']) > 100:
        answer = {"code": 0,
                  "message": "the length of the received data of the 'name' field exceeds the permissible value",
                  "data": None}
        return jsonify(answer)

    if not info_update['password']:
        answer = {"code": 0,
                  "message": "field 'password' is empty",
                  "data": None}
        return jsonify(answer)
    elif not type(info_update['password']) == str:
        answer = {"code": 0,
                  "message": str("invalid data type: [password]"),
                  "data": None}
        return jsonify(answer)
    elif len(info_update['password']) > 50:
        answer = {"code": 0,
                  "message": "the length of the received data of the 'password' field exceeds the permissible value",
                  "data": None}
        return jsonify(answer)

    if not info_update['email']:
        answer = {"code": 0,
                  "message": "field 'email' is empty",
                  "data": None}
        return jsonify(answer)

    # проверка на корректный ввод имейла
    if not check_email(info_update['email']):
        answer = {"code": 0,
                  "message": "incorrect input of the 'email' field",
                  "data": None}
        return jsonify(answer)
    elif len(info_update['email']) > 200:
        answer = {"code": 0,
                  "message": "the length of the received data of the 'email' field exceeds the permissible value",
                  "data": None}
        return jsonify(answer)

    status, info_user = db_update_info_user_2(id_user, info_update)

    if status == -2:
        answer = {"code": 0,
                  "message": "the chosen faculty has no courses/course or faculty is Null",
                  "data": None}
        return jsonify(answer)
    if status == -1:
        answer = {"code": -2,
                  "message": "error while working with DB",
                  "data": None}
        return jsonify(answer)
    # проверка на наличие введенного имейла в БД, если имейл существует - выдать ошибку
    elif status == 0:
        answer = {"code": -1,
                  "message": "email already exists",
                  "data": None}
        return jsonify(answer)
    elif status == 1:
        answer = {"code": 1,
                  "message": "ok",
                  "data": info_user}

    return jsonify(answer)


@app.route(api_user + '/logout')
def logout():
    s_key = request.headers.get('sessionKey')
    id_user = get_id_user(s_key)
    if id_user != -1:
        print(s_key)
        del_session_key(s_key)
        answer = {"code": 1,
                  "message": "ok",
                  "data": None}
        return jsonify(answer)
    answer = {"code": 0,
              "message": "invalid sessionKey",
              "data": None}
    return jsonify(answer)


if __name__ == '__main__':
    app.run(port=5008, host='0.0.0.0')  # 0-65000


# register
# login/entry
# info
# update
# logout
#### !!! session key, структура ответа- одинаковая для одной сущности вне зависимости от запроса
